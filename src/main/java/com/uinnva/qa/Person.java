package com.uinnva.qa;

public final class Person {
    private String fs=null;
    private String ls=null;
     public Person(String firstName, String lastName){
        this.fs = firstName;
        this.ls = lastName;
    }
    public String getFirstName(){
        return fs;
    }
    public String getLastName(){
        return ls;
    }

}
