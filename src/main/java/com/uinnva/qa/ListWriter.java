package com.uinnva.qa;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ListWriter {
    Path filePath = null;
    public ListWriter(Path  filePath) {
        this.filePath  =  filePath;
    }

    public void write (String x,String y,String z){
        try {
            BufferedWriter writer = Files.newBufferedWriter(filePath, StandardCharsets.UTF_8);
            writer.write(x+","+y+","+z);
            writer.flush();
            writer.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
