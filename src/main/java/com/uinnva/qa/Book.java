package com.uinnva.qa;

public class Book {
    private final String title;
    private Book(String title){
        this.title = title;
    }
    public static Book fromTitle(String title){
        System.out.println("========cov title====");
        return new Book(title);
    }

    public String getTitle(){
        return this.title;
    }


//    private final int num;
//    private Book(int num){
//        this.num = num;
//    }
//    public static Book fromNum(int num){
//        System.out.println("========cov num====");
//        return new Book(num);
//    }
//    public int getNum(){
//        return this.num;
//    }
}
