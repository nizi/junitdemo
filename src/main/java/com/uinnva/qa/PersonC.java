package com.uinnva.qa;

public class PersonC {
    String fn;
    String mn;
    String ln;
    public  PersonC(String fn,String mn,String ln){
        this.fn=fn;
        this.mn=mn;
        this.ln=ln;
    }
    public String fullName(){
        if (mn==null|| mn.trim().isEmpty()){
            return String.format("%s %s",fn,ln);
        }
        return String.format("%s %s %s",fn, mn,ln);
    }
}
