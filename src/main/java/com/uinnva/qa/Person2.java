package com.uinnva.qa;

import java.time.LocalDate;
import java.util.Date;

public class Person2 {

    private String fs=null;
    private String ls=null;
    private Gender gd=null;
    private LocalDate dt=null;

    void  Person2(String firstName, String lastName, Gender gender, LocalDate date){
        this.fs = firstName;
        this.ls = lastName;
        this.gd = gender;
        this.dt = date;
    }



    public String getFirstName(){
        return fs;
    }

    public String getLastName(){
        return ls;
    }
    public Gender getGender(){
        return gd;
    }

    public LocalDate getDateOfBirth(){
        return dt;
    }


}
