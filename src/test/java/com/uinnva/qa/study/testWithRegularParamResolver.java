package com.uinnva.qa.study;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class testWithRegularParamResolver {
    @BeforeAll
    @DisplayName("依赖注入BeforeAll")
    static void beforeEach(TestInfo ti){
        System.out.println("===name==="+ti.getDisplayName());
    }

    @ParameterizedTest
    @DisplayName("依赖注入")
    @ValueSource(strings ={"apples","ba"} )
    void testRegReSTest(String arg, TestReporter testReporter){
        testReporter.publishEntry("arg",arg);
    }
    @AfterAll
    static void aferAll(TestInfo testInfo){
        System.out.println("===class ==="+testInfo.getTestClass());
    }
}
