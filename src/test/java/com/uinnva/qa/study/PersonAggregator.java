package com.uinnva.qa.study;

import com.uinnva.qa.Person;
import com.uinnva.qa.PersonC;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;

public class PersonAggregator implements ArgumentsAggregator {
    @Override
    public Object aggregateArguments(ArgumentsAccessor accessor, ParameterContext context){
        return new PersonC(accessor.getString(1),accessor.getString(2),accessor.getString(3));
    }
}
