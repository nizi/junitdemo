package com.uinnva.qa.study;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
//@Disabled
public class TestCase01 {
    @BeforeAll
    static void setup(){
        System.out.println("@BeforeAll" );
    }
    @BeforeEach
     void setupThis(){
        System.out.println("@BeforeEach" );
    }

    @Tag("DEV")
    @Test
    void testCalOne(){
        System.out.println("========test one ");
        Assertions.assertEquals(4,2+2);
    }


    @Test
    @Tag("PROD")
    void testCalTwo(){
        System.out.println("========test two ");
        Assertions.assertEquals(3,1+2);
        assertNotEquals(4,1+2,"计算错误");
    }

    @Disabled
    @Test
    public  void test2(){
        assertTrue(1==1);
    }


    @AfterEach
    void tearThis(){
        System.out.println("@AfterEach");
    }

    @AfterAll
    static void tear(){
        System.out.println("@AfterAll");
    }
}
