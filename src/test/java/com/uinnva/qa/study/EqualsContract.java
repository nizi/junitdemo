package com.uinnva.qa.study;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public interface EqualsContract<T> extends Testable<T>{
    T createNotEqualValue();

    @Test
    default void valueEqualsItSelf(){
        T value = createValue();
        assertEquals(value,value);
    }

    @Test
    default  void valueDoesNotEqualsNull(){
        T value =  createValue();
        assertFalse(value.equals(null));
    }

    @Test
    default void valueDoesNotEqualDifferentValue(){
        T value = createValue();
        T difValue = createNotEqualValue();
        assertNotEquals(value,difValue);
    }
}
