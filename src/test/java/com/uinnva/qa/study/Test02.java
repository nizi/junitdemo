package com.uinnva.qa.study;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Test02 {
    String a="h" ;

//    @Test
    @ParameterizedTest
    @ValueSource(strings = "a")
    void test1(String a){
        System.out.println("a:"+a);
        assertEquals(a,"h");
    }
}
