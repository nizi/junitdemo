package com.uinnva.qa.study;

import java.util.HashMap;
import java.util.Map;

public class DomainNameResolver {

    private Map<String,String> ipByDomainName = new HashMap<String,String>();

    DomainNameResolver(){
        this.ipByDomainName.put("www.baidu.com","1.1.1.2");
        this.ipByDomainName.put("www.google.com","1.1.1.3");
        this.ipByDomainName.put("www.sina.com","1.1.1.4");
    }

    public String getIp(String name){
        return ipByDomainName.get(name);
    }
}
