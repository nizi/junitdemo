package com.uinnva.qa.study;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class MyProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context){
//        return Stream.of("apple","banana").map();
        return Stream.of("apple","banana").map(Arguments::of);

    }
}
