package com.uinnva.qa.study;

import com.sun.org.apache.xerces.internal.xs.StringList;
import com.uinnva.qa.demo.MyExecutable;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.*;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;

public class AssertionsTest {

    @Test
    void testAssertEquals(){
        assertEquals(2,1+1);
        assertEquals(2,1+1,"计算错误");

        Supplier msgSupplier = ()->" test failed";
        assertEquals(1,1+1,msgSupplier);
    }

    @Test
    void testArrayEquals(){
        assertArrayEquals(new int[]{1,2,3},new int[]{1,2,3},"Array Equals Test");
    }

    @Test
    void testArrayEquals2(){
        assertArrayEquals(new int[]{1,2,3},new int[]{1,3,2},"Array Equals Test");
    }
    @Test
    void testArrayEquals3(){
        assertArrayEquals(new int[]{1,2,3},new int[]{1,2,3,4},"Array Equals Test");
    }


    @Test
    void assertIterableTest(){
        Iterable<Integer> listOne = new ArrayList<>(Arrays.asList(1,2,3,4));
        Iterable<Integer> listTwo = new ArrayList<>(Arrays.asList(1,2,3,4));
        Iterable<Integer> listThree = new ArrayList<>(Arrays.asList(1,2,3));
        Iterable<Integer> listFour = new ArrayList<>(Arrays.asList(1,2,3,4,5));

        assertIterableEquals(listOne,listTwo);
//        assertIterableEquals(listOne,listThree);
//        assertIterableEquals(listOne,listFour);
    }

    @Test
    void assertLineDemo(){
        String[][] arr = {{"a,b"},{"c","d"},{"f","e"}};
        String[][] arr2 = {{"a,b"},{"c","d"},{"f","e"}};
        List<String> a = new ArrayList<String>();
        a.add("s");
        a.add("d");

        List<String> b = new ArrayList<String>();
        b.add("s");
        b.add("d");
        assertLinesMatch(a,b);
    }

    @Test
    void testNullDemo(){
        String nullString = null;
        String notNullString = "hel";
        assertNotNull(notNullString);
        assertNull(nullString);
    }

    @Test
    void assertSameDemo(){
//        String a = "hello";
//        String b  = a;
        String c = "hello";
        String a1 = new String();
        String b1 = a1;
        String c1 = new String();

//        assertSame(a1,b1);
//        assertSame(a1,c1);
    }

    @Test
    void test_assertTimeout(){
//        assertTimeout(Duration.ofSeconds(1),new MyExecutable());
//        assertTimeout(Duration.ofSeconds(1),()->{
//            Thread.sleep(2001);
//            System.out.println("sss");
//            return "rs1";
//        });

        assertTimeoutPreemptively(Duration.ofMillis(1003),()->{
            Thread.sleep(2000);
            return "rs";
        });
    }

    @Test
    void assertTrueDemo(){
        boolean tB = true;
        boolean fB = false;

        assertTrue(tB);
//        assertTrue(fB,"this is false");
//        assertTrue(fB,AssertionsTest::msg);
        assertTrue(AssertionsTest::getResult,AssertionsTest::msg);
        assertFalse(fB);
    }

    @Test
    void assertThrowsDemo(){
        Throwable exception = assertThrows(IllegalArgumentException.class,()->{
            throw new IllegalArgumentException("err ");
        });
    }


    @Test
    void testfail(){
//        fail("not good");
        fail(AssertionsTest::msg);
    }

    public static String msg(){
        return "app test fail";
    }

    private static boolean getResult(){
        return false;
    }
}
