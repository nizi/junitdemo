package com.uinnva.qa.study;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.ThrowingConsumer;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DynamicTestDomainName {
    @TestFactory
    Stream<DynamicTest> testNameByIp(){

        List<String> inputList = Arrays.asList("www.baidu.com","www.google.com","www.sina.com");
        List<String> outputList = Arrays.asList("1.1.1.2","1.1.1.3","1.1.1.4");

        Iterator<String> InputGenenrator = inputList.iterator();
        Function<String,String> displayNameGenerator = (input)->"Testing::"+input;
        DomainNameResolver nameResolver = new DomainNameResolver();
        ThrowingConsumer<String> testExecutor = (input)->{
            int id = inputList.indexOf(input);
            assertEquals(outputList.get(id),nameResolver.getIp(input));

        };
        return DynamicTest.stream(InputGenenrator,displayNameGenerator,testExecutor);
    }

    @TestFactory
    Stream<DynamicTest> testName2(){
        List<String> inputList = Arrays.asList("www.baidu.com","www.google.com","www.sina.com");
        List<String> outputList = Arrays.asList("1.1.1.2","1.1.1.3","1.1.1.4");

        DomainNameResolver nameResolver = new DomainNameResolver();
        return inputList.stream().map(input->DynamicTest.dynamicTest("Resolving:::"+input,()->{
            System.out.println(input);
        int id = inputList.indexOf(input);
        assertEquals(outputList.get(id),nameResolver.getIp(input));
        }));
    }
}
