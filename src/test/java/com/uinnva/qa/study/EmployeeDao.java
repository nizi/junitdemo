package com.uinnva.qa.study;

public class EmployeeDao {

    private int id;
    private String name;

    public Employee save(int id ){
        Employee emp = new Employee(id);
        return emp;
    }

    public Employee save(int id ,String name){
        Employee emp = new Employee(id,name);
        return emp;
    }
}
