package com.uinnva.qa.study;

import org.junit.Assert;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeforAllTest {

    @Test
    @DisplayName("before test.")
    @Tag("RtTest")
//    @RepeatedTest(value=3,name = "{displayName}-repetition {currentRepetition} of {totalRepetitions}")
    public void addTest(TestInfo testInfo){
        String s1 = "a";
        Assertions.assertEquals(2,1+1,"计算错误");
//        Assertions.assertEquals("a",s);
        System.out.println(testInfo.getDisplayName());
        System.out.println(testInfo.getTags());
        System.out.println(testInfo.getTestMethod());
        System.out.println(testInfo.getTestClass());
        System.out.println(testInfo.equals(new BeforAllTest()));
    }

//    @Test
//     public  void addTest2( ){
//        Assertions.assertEquals(2,1+1,"计算错误");
//    }

//    @Test
    @DisplayName("add test..")
    @RepeatedTest(value=4,name="{displayName}-repate{currentRepetition} of {totalRepetitions}")
//    @RepeatedTest(value=4,name=RepeatedTest.LONG_DISPLAY_NAME)
//    @RepeatedTest(value=4,name=RepeatedTest.SHORT_DISPLAY_NAME)
    public void testAdd(){
        assertEquals(3,1+2);
    }

//    @BeforeAll
//    public static  void init(){
//        System.out.println("=====initAll======");
//    }

//    @BeforeEach
//    public   void initEach(){
//        System.out.println("======*****init each***=====");
//    }
//
//    @AfterEach
//    public   void afterEach(){
//        System.out.println("======****byeEach****=====");
//    }

//    @AfterAll
//    public  static void afterAll(){
//        System.out.println("======****byeAll****=====");
//    }
}
