package com.uinnva.qa.study;

import org.junit.jupiter.api.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test01 {
    private  static String a ;
    @BeforeAll
    static void init(){
        System.out.println("===****a init==="+a);
        a = "0";
    }

    @Test
    @Order(1)
    void first(){
        System.out.println("===1=="+a);
        a = "hello";

    }

    @Test
    @Order(2)
    void sec(){
        System.out.println("===2=="+a);
        a = "world";

    }

//    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @Nested
    class SubClass{
        String b;
//        @BeforeAll
//        @BeforeEach
         void binit(){
            System.out.println("==b init==");
        }

        @Test
        void testb(){
            System.out.println("====b test1==="+b);
            b="ui";
        }

        @Test
        void testb2(){
            System.out.println("====b test2==="+b);
            b="uinnova";
        }

//        @AfterAll
        @AfterEach
        void btear(){
            System.out.println("==b tear==");
        }
    }

    @AfterAll
    static void tear(){
        System.out.println("===****a tear==="+a);
        a = " ";
    }

}
