package com.uinnva.qa.study;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ResourceLock;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.parallel.ExecutionMode.CONCURRENT;
import static org.junit.jupiter.api.parallel.ExecutionMode.SAME_THREAD;
import static org.junit.jupiter.api.parallel.ResourceAccessMode.READ;
import static org.junit.jupiter.api.parallel.ResourceAccessMode.READ_WRITE;
import static org.junit.jupiter.api.parallel.Resources.SYSTEM_PROPERTIES;

@Execution(CONCURRENT)
//@Execution(SAME_THREAD)
//@Execution(ExecutionMode.CONCURRENT)
public class ShareResourcesDemo {
    private Properties backup;

    @BeforeEach
    void backup(){
        backup = new Properties();
        backup.putAll(System.getProperties());
    }

    @AfterEach
    void restore(){
        System.setProperties(backup);
    }

    @Test
//    @ResourceLock(value=SYSTEM_PROPERTIES,mode = READ)
    void customPropertyIsNotSetByDefault(){
        assertNull(System.getProperty("my.po"));
    }

    @Test
//    @ResourceLock(value = SYSTEM_PROPERTIES,mode = READ_WRITE)
    void cansetCustomPropertyToApple(){
        System.setProperty("my.pop","apple");
        assertEquals("apple",System.getProperty("my.pop"));
    }

    @Test
//    @ResourceLock(value = SYSTEM_PROPERTIES,mode = READ_WRITE)
    void canSetCustomPRopertyToBanana(){
        System.setProperty("my.prop","banana");
        assertEquals("banana",System.getProperty("my.pop"));
    }
}
