package com.uinnva.qa.study;


import org.junit.jupiter.api.BeforeEach;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SuiteDisplayName("JUnit Platform Suite Demo")
@SelectPackages("com.uinnva.qa.study")
public class TestSuit1 {
    @BeforeEach
    void ss(){
        System.out.println("========");
    }
}