package com.uinnva.qa.study;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import static com.uinnva.qa.demo.ExtensionDeployDemo.*;

public class AbstractTest {

    @BeforeAll
    static void createDataBase(){
        beforeAllMethod(AbstractTest.class.getSimpleName()+".createDataBase()---BeforeAll");
    }

    @BeforeEach
    void connectToDatabase() {
        beforeEachMethod(AbstractTest.class.getSimpleName() + ".connectToDatabase()--beforeeach");
    }

    @AfterEach
    void disconnectFromDatabase() {
        afterEachMethod(AbstractTest.class.getSimpleName() + ".disconnectFromDatabase()--aftoreeach");
    }

    @AfterAll
    static void destroyDatabase() {
        afterAllMethod(AbstractTest.class.getSimpleName() + ".destroyDatabase()---AfterAll");
    }


}
