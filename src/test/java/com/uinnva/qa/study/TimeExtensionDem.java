package com.uinnva.qa.study;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class TimeExtensionDem implements AfterTestExecutionCallback, BeforeTestExecutionCallback {


    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
        System.out.println("==========bye=====");
    }

    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) throws Exception {
        System.out.println("==========hello=====");

    }
}
