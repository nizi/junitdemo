package com.uinnva.qa.study;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExtensionDemo {

    @ExtendWith(TimeExtensionDem.class)
    @Test
    void test(){
        assertEquals(1,1);
    }

}
