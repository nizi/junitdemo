package com.uinnva.qa.study;

import com.sun.tracing.dtrace.ProviderAttributes;
import com.uinnva.qa.Book;
import com.uinnva.qa.EnumC;
import com.uinnva.qa.Person;
import com.uinnva.qa.PersonC;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.JavaTimeConversionPattern;
import org.junit.jupiter.params.provider.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.junit.jupiter.params.provider.EnumSource.Mode.MATCH_ALL;

public class ParamTest {

    @ParameterizedTest
    @ValueSource(strings = {"1","3","4"})
    public void testValSource(String s){
        assertTrue(Integer.parseInt(s)<5);
    }

    @ParameterizedTest
//    @NullSource
    @ValueSource(strings = {" ","\t","\n"})
    public void testNullValSource(String s){
        for(int i=0;i<s.length();i++){
        System.out.println("::"+s);
        }
//        assertTrue(s==null );
    }

    @ParameterizedTest
    @EnumSource(TimeUnit.class)
    void testEnum(TimeUnit tu){
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertNotNull(tu);
    }

    @ParameterizedTest
    @EnumSource(value = EnumC.class,mode= MATCH_ALL, names={".*E.*W$"})
    void testEnum(EnumC tu){
//        for(int i=0;i<;i++){
//
//        }
        System.out.println(tu);
        System.out.println(tu+"hell");
        assertNotNull(tu);
    }

    @ParameterizedTest
    @MethodSource("sp")
    void testMethodSource(String arg){
        System.out.println(arg);
        assertNotNull(arg);
    }
    static Stream<String> sp(){
        return Stream.of("app","bama");
    }

    @ParameterizedTest
    @MethodSource
    void sp(String arg){
        System.out.println(arg);
        assertNotNull(arg);
    }
    static Stream<String> sp2(){
        return Stream.of("app2","bama3");
    }

    @ParameterizedTest
    @MethodSource("range")
    void testRangeMethodS(int n){
        assertNotEquals(9, n);
    }

    static IntStream  range(){
        return IntStream.range(0,20).skip(10);
    }


    @ParameterizedTest
//    @RepeatedTest(2)
    @MethodSource("ObjectPara")
    void testS(String str,int n,List<String> ls){
        assertEquals(5,str.length());
        assertTrue(n>=1 && n<=2);
        assertEquals(2,ls.size());
    }


    static Stream<Arguments> ObjectPara(){
        return Stream.of(arguments("apple",1,Arrays.asList("a","b")),
                arguments("lemon",2,Arrays.asList("x","y")));

    }

    @ParameterizedTest
    @MethodSource("com.uinnva.qa.study.ParamTest#tinyStrings")
    void testTingy(String s){
        System.out.println(s);
        assertEquals(s.length(),1);
    }

    static Stream<String> tinyStrings(){
        return Stream.of(".","o","s");
    }

    @ParameterizedTest
    @CsvSource({"hel,1","b,2","c,3","'len world',5"})
    void testCvs(String s,int n){
        assertNotNull(s);
        assertNotEquals(0,n);
    }

    @ParameterizedTest
    @CsvFileSource(resources="/h.csv",numLinesToSkip = 1)
    void testCvsFile(String s,int n){
        assertNotNull(s);
        assertNotEquals(0,n);
    }

    @ParameterizedTest
    @ArgumentsSource(MyProvider.class)
    void testAs(String arg){
        System.out.println(arg);
        assertNotNull(arg);
    }


    @ParameterizedTest
    @ValueSource(booleans = {false})
    void testArgumentsCoversion(boolean s){
        System.out.println(s);
//        assertNotNull(s.name());
    }

    @ParameterizedTest
    @ValueSource(strings = "hell")
    void testCoversion(Book book){
        System.out.println(book.getTitle());
        assertEquals("41",book.getTitle());
    }

//    @ParameterizedTest
//    @ValueSource(ints = {1})
//    void testCoversion2(Book book){
//        System.out.println(book.getNum());
//        assertEquals(41,book.getNum());
//    }

    @ParameterizedTest
    @EnumSource(TimeUnit.class)
//    @ValueSource(strings = "yosd")
    void testExplicitArgConv(@ConvertWith(ToStringArgConverter.class) String arg){
        assertNotNull(TimeUnit.valueOf(arg));
//        assertEquals("y",arg);
    }

    @ParameterizedTest
    @ValueSource(strings = {"01.01.2017", "31.12.2017"})
    void testJavaTimeConver(@JavaTimeConversionPattern("dd.MM.yyyy")LocalDate arg){
        assertEquals(2017,arg.getYear());
    }

    @ParameterizedTest
    @CsvSource({"wang,ni,ni,wang ni ni","zhang,,jie,zhang jie"})
    void fullNameTest(ArgumentsAccessor argumentAccessor){
    String fn = argumentAccessor.getString(0);
    String mn = (String) argumentAccessor.get(1);
    String ln = argumentAccessor.get(2,String.class);
    String en = argumentAccessor.getString(3);
    PersonC p = new PersonC(fn,mn,ln);
    assertEquals(en,p.fullName());
    }

    @ParameterizedTest
    @CsvSource({"wang,ni,ni,wang ni ni","zhang,,jie,zhang jie"})
    void customFullnameAggretor(@AggregateWith(PersonAggregator.class) PersonC person,String expectName){
        assertEquals(expectName,person.fullName());

    }


    @ParameterizedTest
    @MethodSource // hmm, no method name ...
    void isBlank_ShouldReturnTrueForNullOrBlankStringsOneArgument(String input) {
        System.out.println(input);
        assertTrue(input.length()<5);
    }

    private static Stream<String> isBlank_ShouldReturnTrueForNullOrBlankStringsOneArgument() {
        return Stream.of("s", "h", "w");
    }

    @ParameterizedTest
    @CsvSource({"APRIL", "JUNE", "SEPTEMBER", "NOVEMBER"}) // Pssing strings
    void someMonths_Are30DaysLongCsv(Month month) {
        final boolean isALeapYear = false;
        assertEquals(30, month.length(isALeapYear));
    }

    @ParameterizedTest
    @CsvSource({"2018/12/25,2018", "2019/02/11,2019"})
    void testCustomArgumentsProvider(@ConvertWith(SlashyDateConveter.class) LocalDate date,int exp){
        assertEquals(exp,date.getYear());

    }
}
