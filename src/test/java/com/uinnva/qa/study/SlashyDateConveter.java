package com.uinnva.qa.study;

import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ArgumentConverter;

import java.time.LocalDate;

public class SlashyDateConveter implements ArgumentConverter {
    @Override
    public Object convert (Object source, ParameterContext context) throws ArgumentConversionException {
        if(!(source instanceof  String)){
            throw new IllegalArgumentException("can't convert to String");
        }
        String[] parts = ((String) source).split("/");
        int year  = Integer.parseInt(parts[0]);
        int month  = Integer.parseInt(parts[1]);
        int day  = Integer.parseInt(parts[2]);
        return LocalDate.of(year,month,day);

    }

}
