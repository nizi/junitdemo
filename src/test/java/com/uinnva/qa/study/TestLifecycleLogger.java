package com.uinnva.qa.study;

import org.junit.jupiter.api.*;

import java.util.logging.Logger;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public interface TestLifecycleLogger {

    Logger log =  Logger.getLogger(TestLifecycleLogger.class.getName());

    @BeforeAll
    default  void beforeAll(){
        log.info("before all test");
    }

    @AfterAll
    default void afterAll(){
        log.info("afterAll all test");
    }

    @BeforeEach
    default void beforeEach(TestInfo testInfo){
        log.info(()->String.format("************execute method [%s]",testInfo.getDisplayName()));
    }

    @AfterEach
    default void afterEach(TestInfo testInfo){
        log.info(()->String.format("************finished method [%s]",testInfo.getDisplayName()));
    }

}
