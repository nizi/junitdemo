package com.uinnva.qa.study;

import com.uinnva.qa.demo.MyExecutable;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.ThrowingConsumer;

import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.uinnva.qa.StringUtils.isPalindrome;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.DynamicContainer.dynamicContainer;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

public class dynamicTests {

    @TestFactory
    Collection<DynamicTest> dynamicTestCollection(){
        return Arrays.asList(
                dynamicTest("dimpe",()->assertTrue(true)),
                dynamicTest("excutable",new MyExecutable()),
                dynamicTest("exception",()->{throw new Exception("Exception");}),
                dynamicTest("dynamic test-2",()->assertTrue(true))
        );
    }

    @TestFactory
    List<String> dynamicTestReturnException(){
        return Arrays.asList("hello");
    }

    @TestFactory
    Collection<DynamicTest> dynamicTestFromCollection(){
        return Arrays.asList(
                dynamicTest("1 dynamic test",()->assertTrue(isPalindrome("true"),"=====")),
                dynamicTest("2 dynamic test",()->assertEquals(2,1+1,"===***==")));
    }
    @TestFactory
    Iterable<DynamicTest> dynamicTestFromIterable(){
        return Arrays.asList(
                dynamicTest("1 dynamic test",()->assertTrue(isPalindrome("true"),"=====")),
                dynamicTest("2 dynamic test",()->assertEquals(2,1+1,"===***==")));
    }

    @TestFactory
    Iterator<DynamicTest> dynamicTestFromIterator(){
        return Arrays.asList(
                dynamicTest("1 dynamic test",()->assertTrue(isPalindrome("true"),"=====")),
                dynamicTest("2 dynamic test",()->assertEquals(2,1+1,"===***=="))).iterator();
    }

    @TestFactory
    DynamicTest[] dynamicTestFromDynamicTest(){
        return new DynamicTest[]{
                dynamicTest("1 dynamic test",()->assertTrue(isPalindrome("true"),"=====")),
                dynamicTest("2 dynamic test",()->assertEquals(2,1+1,"===***=="))
        };
    }

    @TestFactory
    Stream<DynamicTest> dynamicTestFromDynamicStream(){
        return Stream.of("hell","world","mom","daa").map(
                text->dynamicTest(text,()->assertTrue(isPalindrome(text)))
        );
    }

    @TestFactory
    Stream<DynamicTest> dynamicTestFromDynamicIntStream(){
        return IntStream.iterate(0,n->n+2).limit(10).mapToObj(
                n->dynamicTest("test "+n,()->assertTrue(n%2==0))
        );
    }
    @TestFactory
    Stream<DynamicTest> generateRandomNumberTest(){
        Iterator<Integer> inputGenerator = new Iterator<Integer>() {

            Random rm = new Random();
            int current;

            @Override
            public boolean hasNext() {
                current = rm.nextInt(100);
                System.out.println(current);
                return current%7!=0;
            }

            @Override
            public Integer next() {
                return current;
            };
        };
        Function<Integer,String> displayNameGenerator = (input)->"input"+input;
        ThrowingConsumer<Integer> textExecutor = (input) ->assertTrue(input % 7!=0);
        return DynamicTest.stream(inputGenerator,displayNameGenerator,textExecutor);
    }

    @TestFactory
    Stream<DynamicNode> dynamicTestWithContainers(){
        return Stream.of("A","B","C")
                .map(input->dynamicContainer("Container "+input,Stream.of(
                        dynamicTest("not null",()->assertNotNull(input)),
                        dynamicContainer("properties",Stream.of(
                                dynamicTest("length>0",()->assertTrue(input.length()>0)),
                                dynamicTest("not empty",()->assertFalse(input.isEmpty()))
                        ))
                )));
    }

    @TestFactory
    DynamicNode dynamicNodeSingleTest(){
        return dynamicTest("pop is palindrome",()->assertTrue(isPalindrome("pop")));
    }

    @TestFactory
    DynamicNode dynamicNodeSingleContainer(){
        return dynamicContainer("palindromes",
                Stream.of("racer","dadf","mom").map(text->dynamicTest(text,()->assertTrue(isPalindrome(text)))));
    }

//    @TestFactory
//    DynamicNode dynamicNodeWithURl(){
//        return dynamicTest("url test ","com.uinnva.qa.study",MyExcutable);
//    }
}
