package com.uinnva.qa.study;

import com.uinnva.qa.demo.MyTestTemplateInvocationContextProvider;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.JavaTimeConversionPattern;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;
import org.junit.jupiter.params.provider.*;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.DynamicContainer.dynamicContainer;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;

public class ParamTestDemo {

    @ParameterizedTest
    @ValueSource(strings = {"hell","worl"})
    void test1(String s){
        assertTrue(s.length()<5);
    }

    @ParameterizedTest
    @EnumSource(TimeUnit.class)
    void test2(TimeUnit timeUnit){
        assertNotNull(timeUnit);
    }

    @ParameterizedTest
    @EnumSource(value = TimeUnit.class,mode=EXCLUDE,names = {"DAYS","HOURS"})
    void test3(TimeUnit timeUnit){
        assertTrue(timeUnit.name().length()>5);
    }


    @ParameterizedTest
    @MethodSource("mes")
    void testMd(String msg){
        assertNotNull(msg);
    }


    static Stream<String> mes(){
        return Stream.of("foo","bar");
    }


    @ParameterizedTest
    @MethodSource("intStream")
    void testwithMethod(int n){
        assertTrue(n>9);
    }

    static IntStream intStream(){
        return IntStream.range(0,20).skip(10);
    }

    @ParameterizedTest
    @MethodSource("stringArgProvider")
    void testArg(String s, int n, List<String> li){
        assertEquals(4,s.length());
        assertTrue(n>0);
        assertTrue(li.get(1).contains("w"));
    }


    static Stream<Arguments> stringArgProvider(){
        return Stream.of(
                Arguments.of("foo5",1,Arrays.asList("hw","wei")),
                Arguments.of("foo2",3,Arrays.asList("world","wu")));
    }

    @ParameterizedTest
    @ArgumentsSource(MyArgProvider.class)
    void testArg(String s){
        assertTrue(s.length()<4);
    }


    static class MyArgProvider implements ArgumentsProvider{
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
            return Stream.of("foo","bar").map(Arguments::of);
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"1","2"})
    void testConv(int i){
        assertTrue(i<3);
    }
//    void testConv(String i){
//        assertTrue(i.length()<3);
//    }

    @ParameterizedTest
    @ValueSource(ints = {1,3})
    void testCusCov (@ConvertWith(CusCov.class) String s ){
        assertTrue(!s.isEmpty());
    }

    static class CusCov extends SimpleArgumentConverter{

        @Override
        protected Object convert(Object source, Class<?> targeType) throws ArgumentConversionException {
            assertEquals(String.class,targeType,"cann't convert to String");
            return String.valueOf(source);
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"01.01.2017"})
    void JvmCov (@JavaTimeConversionPattern("dd.MM.yyyy") LocalDate arg){

        assertEquals(2017,arg.getYear());
    }
    @TestFactory
    Stream<DynamicNode> dynamicTestsWithContainers() {
        return Stream.of("A", "B", "C")
                .map(input -> dynamicContainer("Container " + input, Stream.of(
                        dynamicTest("not null", () -> assertNotNull(input)),
                        dynamicContainer("properties", Stream.of(
                                dynamicTest("length > 0", () -> assertTrue(input.length() > 0)),
                                dynamicTest("not empty", () -> assertFalse(input.isEmpty()))
                        ))
                )));
    }

    @TestFactory
    Stream<DynamicTest> dynamicTestsFromIntStream() {
        // Generates tests for the first 10 even integers.
        return IntStream.iterate(0, n -> n + 2).limit(10)
                .mapToObj(n -> dynamicTest("test" + n, () -> assertTrue(n % 2 == 0)));
    }


    final List<String> fruits = Arrays.asList("apple", "banana", "lemon");

    @TestTemplate
    @ExtendWith(MyTestTemplateInvocationContextProvider.class)
    void testTemplate(String fruit) {
        assertTrue(fruits.contains(fruit));
    }


}
