package com.uinnva.qa.study;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ExampleTestCase {

//    private final Calculator calculator = new Calculator();

    @Test
    @Disabled("for demonstration purposes")
    @Order(1)
    void skippedTest() {
        // skipped ...
        assertEquals(1,1);
    }

    @Test
    @Order(2)
    void succeedingTest() {
        assertEquals(42, 6*7);
    }

    @Test
    @Order(3)
    void abortedTest() {
//        assertEquals(1,2);
        assertTrue(1>2);
        assumeTrue("abc".contains("Z"), "abc does not contain Z");
        // aborted ...
    }

    @Test
    @Order(4)
    void failingTest() {
        // The following throws an ArithmeticException: "/ by zero"
       System.out.println(1/1);
    }
}
