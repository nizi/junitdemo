package com.uinnva.qa.study;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.uinnva.qa.demo.ExtensionDeployDemo.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@ExtendWith({Extension1.class,Extension2.class})
public class DataBaseTestDemo extends AbstractTest {
    @BeforeAll
    static void beforeAll(){
        beforeAllMethod(DataBaseTestDemo.class.getSimpleName()+".beforeAll()");
    }

    @BeforeEach
    void insertTestDataIntoDatabase() {
        beforeEachMethod(getClass().getSimpleName() + ".insertTestDataIntoDatabase()---BeforeEach");
    }

    @Test
    @Disabled("not test")
    void testDatabaseFunctionality() {
        testMethod(getClass().getSimpleName() + ".testDatabaseFunctionality()");
    }

    @Test
    void failingTest(){
        assertNotNull(1/0);
    }

    @Test
    void succeedTest(){
        assertNotNull(1);
    }

    @Test
    void abortedTest(){
        assumeTrue("abc".contains("z"),"abd don't contain z");
        fail("test should hava been aborted");
    }

    @AfterEach
    void deleteTestDataFromDatabase() {
        System.out.println("------------------");
        afterEachMethod(getClass().getSimpleName() + ".deleteTestDataFromDatabase()---AfterEach");
    }

    @AfterAll
    static void afterAll() {
        beforeAllMethod(DataBaseTestDemo.class.getSimpleName() + ".afterAll()");
    }
}
