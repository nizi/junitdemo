package com.uinnva.qa.study;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class NestedDemo {

    @Test
    void testD(){
        assertEquals(1,1+1);
    }

    @Nested
    class InnerClass {
        @BeforeEach
        void beforeAll(){
            System.out.println("----beforeAll---");
        }
        @Test
        void testMethod1(){
            fail("=====innner class====");
        }

        @Nested
        class MostClass {
            void moTest(){
                assertEquals(1,1+1);
            }
        }
    }
}
