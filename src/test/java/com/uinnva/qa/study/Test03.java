package com.uinnva.qa.study;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("testinfo demo")
public class Test03 {
//    Test03(TestInfo testInfo){
//        assertEquals("testinfo demo",testInfo.getDisplayName());
//    }

    @BeforeEach
    void each(TestInfo testInfo){
        String disName = testInfo.getDisplayName();
        assertTrue(disName.equals("test1") || disName.equals("test2"));
    }

    @Test
    @DisplayName("test1")
    @Tag("tag1")
    void test1(TestInfo testInfo){
        assertTrue(testInfo.getTags().contains("tag1"));

    }
    @Test
    @DisplayName("test2")
    @Tag("tag2")
    void test2(TestInfo testInfo){
        assertTrue(testInfo.getTags().contains("tag2"));

    }

}
