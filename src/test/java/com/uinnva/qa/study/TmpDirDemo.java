package com.uinnva.qa.study;

import com.uinnva.qa.ListWriter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TmpDirDemo {
    @Test
    void writeItemsToFile (@TempDir Path tmDir) throws IOException{
        Path file = tmDir.resolve("test.txt");
        Path file2 = tmDir.resolve("2.txt");
        new ListWriter(file).write("a","b","c");
        new ListWriter(file2).write("1","2","c");
        assertEquals(singletonList("a,b,c"), Files.readAllLines(file));
        assertEquals(singletonList("1,2,c"), Files.readAllLines(file2));
    }
}
