package com.uinnva.qa.study;

import org.junit.Test;
import org.junit.platform.engine.*;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestPlan;
import org.junit.platform.launcher.core.LauncherConfig;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

import static org.junit.platform.engine.discovery.ClassNameFilter.includeClassNamePatterns;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

public class LauncherTest {

    @Test
    public void testLauncher(){

        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request().selectors(
                selectPackage("com.uinnva.qa.study"),
                selectClass(DataBaseTestDemo.class)
        ).filters(
                includeClassNamePatterns(".*TestDemo")
        ).build();
        Launcher launcher = LauncherFactory.create();
        TestPlan testPlan = launcher.discover(request);

        SummaryGeneratingListener listener = new SummaryGeneratingListener();
        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request, new TestExecutionListener[] {listener});
//        TestExecutionSummary summary =  listener.getSummary();
        System.out.println(listener.getSummary().getTestsSucceededCount());
    }


    @Test
    public void testLauncher2(){
        SummaryGeneratingListener listener = new SummaryGeneratingListener();
        LauncherConfig launcherConfig = LauncherConfig.builder()
                .enableTestEngineAutoRegistration(false)
                .enableTestExecutionListenerAutoRegistration(false)
//                .addTestEngines("/META-INF/services/junit-jupiter-engine")
                .addTestExecutionListeners()
                .build();
        Launcher launcher = LauncherFactory.create(launcherConfig);
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectPackage("com.uinnva.qa.study")
                        ,selectClass(DataBaseTestDemo.class))
                .build();
        launcher.execute(request);
        System.out.println(listener.getSummary().getTimeFinished());
    }



}
