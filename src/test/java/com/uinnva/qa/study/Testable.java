package com.uinnva.qa.study;

public interface Testable<T> {

    T createValue();
}