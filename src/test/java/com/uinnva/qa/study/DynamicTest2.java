package com.uinnva.qa.study;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DynamicTest2 {
    @TestFactory
    Collection<DynamicTest> dyanamicTestsWithCollection(){
        return Arrays.asList(
                DynamicTest.dynamicTest("add test1",()->assertEquals(1,1)),
                DynamicTest.dynamicTest("add test2",()->assertTrue(1<2))
                );
    }

    @TestFactory
    Iterator<DynamicTest> dyanamicTestsWithIterator(){
        return Arrays.asList(
                DynamicTest.dynamicTest("add test1",()->assertEquals(1,1)),
                DynamicTest.dynamicTest("add test2",()->assertTrue(1<2))
        ).iterator();
    }

    @TestFactory
    Iterable<DynamicTest> dyanamicTestsWithIterable(){
        return Arrays.asList(
                DynamicTest.dynamicTest("add test1",()->assertEquals(1,1)),
                DynamicTest.dynamicTest("add test2",()->assertTrue(1<2))
        );
    }

    @TestFactory
    Stream<DynamicTest> dynamicTestStream(){
        return IntStream.iterate(0,n->n+2).limit(10).mapToObj(n->DynamicTest.dynamicTest(
                "test"+n,()->assertTrue(n%2==0)
        ));
    }

    @TestFactory
    Stream<DynamicTest> dynamicTestStream2(){
        List<Employee> inputList = Arrays.asList(new Employee(1,"nizi"),new Employee(2),new Employee(3,"nizi3"));
        EmployeeDao dao =  new EmployeeDao();
        Stream<DynamicTest> saveEmployee = inputList.stream().map(emp->DynamicTest.dynamicTest("saveEmployee:"+emp.toString(),()->{
                    Employee rd = dao.save(emp.getId());
            assertEquals(rd.getId(),emp.getId());
        }));

        Stream<DynamicTest> saveEmployeeByNameAndId = inputList.stream().filter(emp->!emp.getName().isEmpty())
                                    .map(emp->DynamicTest.dynamicTest(emp.toString(),()->{
                                        Employee rd = dao.save(emp.getId(),emp.getName());
                                        assertEquals(rd.getId(),emp.getId());
                                        assertEquals(rd.getName(),emp.getName());
                                    }));
        Stream<DynamicTest> tmp = inputList.stream().map(emp->DynamicTest.dynamicTest("test",()->{assertTrue(emp.getName().contains("nizi"));}));
//        return Stream.concat(saveEmployee,saveEmployeeByNameAndId);
        return Stream.concat(saveEmployee,tmp);
    }
}
