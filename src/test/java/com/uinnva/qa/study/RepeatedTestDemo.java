package com.uinnva.qa.study;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.TestInfo;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RepeatedTestDemo {
    private Logger logger  = Logger.getLogger(RepeatedTestDemo.class.getName());
    @BeforeEach
    void beforeEach(TestInfo testInfo, RepetitionInfo repetitionInfo){
        int CurrentRepetition = repetitionInfo.getCurrentRepetition();
        String testMethod  = testInfo.getTestMethod().get().getName();
        logger.info(String.format("method %s currentRe %d",testMethod,CurrentRepetition));
    }

    @RepeatedTest(5)
    void reDemo(RepetitionInfo repetitionInfo){
        assertEquals(5,repetitionInfo.getTotalRepetitions());
    }
}
