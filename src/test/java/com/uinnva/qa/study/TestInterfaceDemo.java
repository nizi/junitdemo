package com.uinnva.qa.study;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestReporter;

import static org.junit.jupiter.api.Assertions.assertEquals;

//public class TestInterfaceDemo implements TestLifecycleLogger,DynamicTestDemo2 {
public class TestInterfaceDemo implements TestLifecycleLogger,TestDynamicTestDemo,TimeExecutionLogger {
    @BeforeAll
    void  initAll(TestReporter testReporter){
        testReporter.publishEntry("----------init all-----");
    }

    @Test
    void test(){
        System.out.println("==========*******========");
        assertEquals(4,2*2);
    }

    @AfterAll
    void  afterAll(TestReporter testReporter){
        testReporter.publishEntry("----------after all-----");
    }
}
