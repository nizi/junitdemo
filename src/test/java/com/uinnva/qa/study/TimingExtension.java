package com.uinnva.qa.study;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.*;

import java.lang.reflect.Method;
import java.util.logging.Logger;

public class TimingExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {

    Logger logger  = Logger.getLogger(TimingExtension.class.getName());
    private static  final String START_TIME = "start time";

    @Override
    public void beforeTestExecution(ExtensionContext context) throws Exception{
        getStore(context).put(START_TIME,System.currentTimeMillis());
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception{
        Method method = context.getRequiredTestMethod();
        long startTime = getStore(context).remove(START_TIME,long.class);
        long duration = System.currentTimeMillis() - startTime;
//        String info = String.format("============method [%s] takes %s ms============",method.getName(),duration);
//        logger.info(info);
        logger.info(()->
                String.format("============method [%s] takes %s ms============",method.getName(),duration));

    }

    private Store getStore(ExtensionContext context){
        return context.getStore(Namespace.create(getClass(),context.getRequiredTestMethod()));
    }
}
