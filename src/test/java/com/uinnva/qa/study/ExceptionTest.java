package com.uinnva.qa.study;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExceptionTest {
    @Test
    void testException(){
        Assertions.assertThrows(NumberFormatException.class,()->{
            Integer.parseInt("sds0");
        });
    }

//    @Test
//    void testExceptionFail(){
//        Assertions.assertThrows(NumberFormatException.class,()->{
//            Integer.parseInt("9");
//        });
//    }
}
