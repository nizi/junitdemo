package com.uinnva.qa.study;

import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ToStringArgConverter extends SimpleArgumentConverter {

    @Override
    protected Object convert(Object source,Class<?> targetType){
        assertEquals(String.class,targetType,"can not convert to String");
        return String.valueOf(source);
    }
}
//
//public class ToIntConverter extends SimpleArgumentConverter{
//    @Override
//    protected Object convert(Object source,Class<?> targetType){
//        assertEquals(Object.class,targetType,"can not conver to int");
//        return Integer.valueOf(source);
//    }
//}