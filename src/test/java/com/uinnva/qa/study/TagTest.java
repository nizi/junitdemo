package com.uinnva.qa.study;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

@Tag("dev")
public class TagTest {

    @Test
    @Tag("usermanage")
    @Tag("prod")
    void testCaseA(TestInfo testInfo){
    System.out.println("========");
    }

    @Test
    @Tag("u2")
    @Tag("prod")
    void testCaseB(TestInfo testInfo){
        System.out.println("========");
    }
}
