package com.uinnva.qa.study;

import org.junit.jupiter.api.Test;
import org.junit.platform.testkit.engine.EngineTestKit;
import org.junit.platform.testkit.engine.Events;
import org.opentest4j.TestAbortedException;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectMethod;
import static org.junit.platform.testkit.engine.EventConditions.*;
import static org.junit.platform.testkit.engine.TestExecutionResultConditions.instanceOf;
import static org.junit.platform.testkit.engine.TestExecutionResultConditions.message;

public class TestCaseStatisticsDemo {
    @Test
    void verifyCon(){
        EngineTestKit.engine("junit-jupiter")
                .selectors(selectClass(DataBaseTestDemo.class))
                .execute()
                .containers()
                .assertStatistics(stats->stats.started(3).succeeded(3));

    }

    @Test
    void verifyCon2(){
        EngineTestKit.engine("junit-jupiter")
                .selectors(selectClass(DataBaseTestDemo.class))
                .execute()
                .tests()
                .assertStatistics(stats->stats.started(3).succeeded(3));

    }

    @Test
    void verifyStats3(){
        String methodName = "testDatabaseFunctionality";
       Events tv = EngineTestKit
                .engine("junit-jupiter")
                .selectors(selectClass(DataBaseTestDemo.class))
                .execute()
                .tests()
                .assertStatistics(stats->stats.succeeded(0));
            tv.assertThatEvents().haveExactly(1,event(test(methodName),
                    skippedWithReason("not test")));

//        Events testEvents = EngineTestKit
//                .engine("junit-jupiter")
//                .selectors(selectMethod(DataBaseTestDemo.class, methodName))
//                .execute()
//                .tests();
//
//        testEvents.assertStatistics(stats -> stats.skipped(1));
//
//        testEvents.assertThatEvents()
//                .haveExactly(1, event(test(methodName),
//                        skippedWithReason("for demonstration purposes")));
    }

    @Test
    void verifyFailed(){
        EngineTestKit.engine("junit-jupiter")
                .selectors(selectClass(DataBaseTestDemo.class))
                .execute()
                .tests()
                .assertThatEvents().haveExactly(
                        1,event(test("failingTest"),
                        finishedWithFailure(instanceOf(ArithmeticException.class),message("/ by zero1"))));

    }

    @Test
    void testAll(){
        EngineTestKit.engine("junit-jupiter")
                .selectors(selectClass(DataBaseTestDemo.class))
                .execute()
                .all()
//                .debug(System.out.)
                .assertEventsMatchExactly(
                        event(engine(),started()),
                        event(container(DataBaseTestDemo.class),started()),
                        event(test("succeedTest"),started()),
                        event(test("succeedTest"),finishedSuccessfully()),
                        event(test("testDatabaseFunctionality"),skippedWithReason("not test")),
                        event(test("failingTest"),started()),
                        event(test("failingTest"),finishedWithFailure(
                                instanceOf(ArithmeticException.class),message("/ by zero"))),
                        event(test("abortedTest"),started()),
                        event(test("abortedTest"),abortedWithReason(
                                instanceOf(TestAbortedException.class),message(m->m.contains("abd don't contain z")))),

                        event(container(),finishedSuccessfully()),
                        event(engine(),finishedSuccessfully())


                );
    }
}
