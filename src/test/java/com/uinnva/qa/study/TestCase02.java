package com.uinnva.qa.study;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

public class TestCase02 {
    @Test
    void testOneDev(){
        System.setProperty("ENV","DEV");
        assumeTrue("DEV".equals(System.getProperty("ENV")),TestCase02::message);
//        Assumptions.assumeFalse("DEV".equals(System.getProperty("ENV")),TestCase02::message);
//        assertEquals(1,1+1);
    }
//
//    @Test
////    @Disabled
//    void testOneProd(){
//        System.setProperty("ENV","PROD");
////        Assumptions.assumeTrue("PROD".equals(System.getProperty("ENV")),TestCase02::say2);
////        Assertions.fail("测试失败",(new Exception().getCause()));
//    }


    @Test
    public void assertAllDemo(){
        assertAll("numbers",()->assertEquals(1,1+1),
                ()->assertEquals(2,1+1));
    }

    @Test
    void testAssertTrue() {
        assertTrue("FirstName".startsWith("F"));
        assertTrue(true,()->{
            throw new IllegalArgumentException("Illegal Argument Exception occured");
        });
//        assertEquals("Illegal Argument Exception occured", new Exception().getMessage());
    }

    @Test
    void testTrue(){
//        assumeTrue(2==1+1);
        assumeFalse(1==1+1);
        System.out.println("assume teest.......");
    }

    @Test
    void testThat(){
//        assumeTrue(2==1+1);
        assumingThat(2==1+1,()->{
            assertEquals(1,10);
            System.out.println("0000000000000000");
        });
        System.out.println("assume teest.......");
    }


    @Nested
    class MostClass {
        void moTest(){
            assertEquals(1,1+1);
        }
    }


    private static String message(){
        return "Test failed";
    }
      static String say2(){
//        return "say hello....";
//         System.out.println("say hello....");
        return "1";
    }

}
