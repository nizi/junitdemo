package com.uinnva.qa.study;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

 interface TestDynamicTestDemo {

    @TestFactory
     default Collection<DynamicTest> dynamicDemo(){
        System.out.println("=========dynamicDemo=========");
        return Arrays.asList(
                dynamicTest("test 1",()->assertTrue(1<2)),
                dynamicTest("test 2",()->assertEquals(2,2))
        );
    }
}
