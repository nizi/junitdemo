package com.uinnva.qa.Tarsier;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import sun.net.www.http.HttpClient;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCase001 {

    @Test
    void testBaidu(){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://192.168.1.82/vmdb-sso/rsm/srv/source/queryLoginLogo");

        try {
            CloseableHttpResponse response  =  httpClient.execute(httpPost);
            String result  = (EntityUtils.toString(response.getEntity()));
            JSONObject jsonObject = (JSONObject) JSON.parse(result);
            assertTrue(jsonObject.getJSONArray("data").size()==5);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
