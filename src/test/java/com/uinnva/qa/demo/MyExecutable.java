package com.uinnva.qa.demo;

import org.junit.jupiter.api.function.Executable;

public class MyExecutable implements Executable {
    @Override
    public void execute() throws Throwable{
        System.out.println("Hello world");
        Thread.sleep(2000);
    }
}
