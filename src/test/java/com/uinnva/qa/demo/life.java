package com.uinnva.qa.demo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestReporter;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class life {
    @BeforeEach
    private void beforeEach(TestInfo testInfo) {
        // ...
    }

    @ParameterizedTest
    @ValueSource(strings = { "apple, 1", "banana, 2", "'lemon, lime', 3" })
    void testWithRegularParameterResolver(String argument, TestReporter testReporter) {
        testReporter.publishEntry("argument", argument);
    }

    @AfterEach
    void afterEach(TestInfo testInfo) {
        // ...
    }
}
