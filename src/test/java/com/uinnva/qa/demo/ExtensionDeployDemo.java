package com.uinnva.qa.demo;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.time.LocalDate;

public   class ExtensionDeployDemo {

    static Logger logger = Logger.getLogger(ExtensionDeployDemo.class);
    public static void  beforeEachCallback(String o){
        logger.info(o);
    }

    public static void  afterEachCallback(String o){
        logger.info(o);
    }

    public static void  beforeAllMethod(String o){
      logger.info(o);
    }

    public static void  afterAllMethod(String o){
        logger.info(o);
    }

    public static void  beforeEachMethod(String o){
        logger.info(o);
    }

    public static void  afterEachMethod(String o){
        logger.info(o);
    }

    public static void  testMethod(String o){
        logger.info(o);
    }





}
