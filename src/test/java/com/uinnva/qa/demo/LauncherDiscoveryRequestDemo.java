package com.uinnva.qa.demo;

import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.TestPlan;

import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;

import static org.junit.platform.engine.discovery.ClassNameFilter.includeClassNamePatterns;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

public class LauncherDiscoveryRequestDemo {
   LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
            .selectors(
                    selectPackage("com.uinnva.qa.215ExternalMethodSourceDemo"),
                    selectClass(MyTestClass.class)
            )
            .filters(
                    includeClassNamePatterns(".*Tests")
            )
            .build();

    Launcher launcher = LauncherFactory.create();

    TestPlan testPlan = launcher.discover(request);
}
