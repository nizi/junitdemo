package com.uinnva.qa.demo;

public interface Testable <T>{

    T createValue();
}
