package com.uinnva.qa.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;

import java.awt.event.WindowStateListener;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.condition.OS.LINUX;
import static org.junit.jupiter.api.condition.OS.WINDOWS;

public class OSDemo {
    @Test
    @EnabledOnOs(WINDOWS)
    void onWins(){
        assertEquals(1,1+2);
    }
    @EnabledOnOs(LINUX)
    void onLinux(){
        assertEquals(1,1);
    }
}
