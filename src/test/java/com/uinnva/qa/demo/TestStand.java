package com.uinnva.qa.demo;


import org.junit.jupiter.api.*;

public class TestStand {
    @BeforeAll
    static void initAll(){
       System.out.println("init all");
    }

    @BeforeEach
    void init(){
        System.out.println("====before each======");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    void firdt(){
        System.out.println("1");
    }
    @Test
    void firdt2(){
        System.out.println("2");
    }

    @Test
    @Disabled("disable")
    void firdt3(){
        System.out.println("disable");
    }

    @AfterEach
    void afterEach(){
        System.out.println("===aftereach==");
    }
    @AfterAll
    static void   tearDownAll(){
        System.out.println("====afterAll===");
    }
}
