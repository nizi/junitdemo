package com.uinnva.qa.demo;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import static com.uinnva.qa.demo.ExtensionDeployDemo.afterEachCallback;
import static com.uinnva.qa.demo.ExtensionDeployDemo.beforeEachCallback;

public class Extension2 implements BeforeEachCallback, AfterEachCallback {

    @Override
    public void beforeEach(ExtensionContext context) {
        beforeEachCallback("demo\\Extension12\\eforeEach");
    }

    @Override
    public void afterEach(ExtensionContext context) {
        afterEachCallback("demo\\Extension12\\atforeEach");
    }

}