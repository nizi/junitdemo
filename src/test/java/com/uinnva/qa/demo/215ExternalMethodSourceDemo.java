package com.uinnva.qa.demo;

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import com.uinnva.qa.Gender;
import com.uinnva.qa.Person2;
import com.uinnva.qa.demo.Book;
import com.uinnva.qa.demo.MyArgumentsProvider;
import com.uinnva.qa.demo.ToStringArgumentConverter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.JavaTimeConversionPattern;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.api.Assertions.*;

class ExternalMethodSourceDemo {

    @ParameterizedTest
    @MethodSource("com.uinnva.qa.StringsProviders#tinyStrings")
    void testWithExternalMethodSource(String tinyString) {
       System.out.println(tinyString);
    }
}

    class StringsProviders {

        static Stream<String> tinyStrings() {
            return Stream.of(".", "oo", "OOO");
        }

        @ParameterizedTest
        @CsvSource({
                "apple,         1",
                "banana,        2",
                "'lemon, lime', 0xF1"
        })
        void testWithCsvSource(String fruit, int rank) {
            System.out.println(fruit+" "+rank);
            assertNotNull(fruit);
            assertNotEquals(0, rank);
        }

        @ParameterizedTest
        @CsvFileSource(resources = "/column.csv", numLinesToSkip = 1)
        void testWithCsvFileSource(String country, int reference) {
            assertNotNull(country);
            assertNotEquals(0, reference);
        }

        @ParameterizedTest
        @ArgumentsSource(MyArgumentsProvider.class)
        void testWithArgumentsSource(String argument) {
            assertNotNull(argument);
        }

        @ParameterizedTest
        @ValueSource(strings = "SECONDS")
        void testWithImplicitArgumentConversion(TimeUnit argument) {
            assertNotNull(argument.name());
            System.out.println(argument.name());
        }

        @ParameterizedTest
        @ValueSource(strings = "42 Cats")
        void testWithImplicitFallbackArgumentConversion(Book book) {
            assertEquals("42 Cats", book.getTitle());
        }

        @ParameterizedTest
        @EnumSource(TimeUnit.class)
        void testWithExplicitArgumentConversion(
                @ConvertWith(ToStringArgumentConverter.class) String argument) {

            assertNotNull(TimeUnit.valueOf(argument));
        }

        @ParameterizedTest
        @ValueSource(strings = { "01.01.2017", "31.12.2017" })
        void testWithExplicitJavaTimeConverter(
                @JavaTimeConversionPattern("dd.MM.yyyy") LocalDate argument) {

            assertEquals(2017, argument.getYear());
        }



//        @ParameterizedTest
//        @CsvSource({
//                "Jane, Doe, F, 1990-05-20",
//                "John, Doe, M, 1990-10-22"
//        })


//        void testWithArgumentsAccessor(ArgumentsAccessor arguments) {
//            Person2 person = new Person2(arguments.getString(0),
//                    arguments.getString(1),
//                    arguments.get(2, Gender.class),
//                    arguments.get(3, LocalDate.class));
//
//            if (person.getFirstName().equals("Jane")) {
//                assertEquals(Gender.F, person.getGender());
//            }
//            else {
//                assertEquals(Gender.M, person.getGender());
//            }
//            assertEquals("Doe", person.getLastName());
//            assertEquals(1990, person.getDateOfBirth().getYear());
//        }

//        @ParameterizedTest
//        @CsvSource({
//                "Jane, Doe, F, 1990-05-20",
//                "John, Doe, M, 1990-10-22"
//        })
//        void testWithArgumentsAggregator(@AggregateWith(PersonAggregator.class) Person2 person2) {
//            // perform assertions against person
//            assertEquals("Doe", person2.getLastName());
//            assertEquals(1990, person2.getDateOfBirth().getYear());
//        }
//
//        @ParameterizedTest
//        @CsvSource({
//                "Jane, Doe, F, 1990-05-20",
//                "John, Doe, M, 1990-10-22"
//        })
//        void testWithCustomAggregatorAnnotation(@CsvToPerson Person2 person2) {
//            // perform assertions against person
//            assertEquals("Doe", person2.getLastName());
//            assertEquals(1990, person2.getDateOfBirth().getYear());
//        }

        @DisplayName("Display name of container")
        @ParameterizedTest(name = "{index} ==> fruit=''{0}'', rank={1}")
        @CsvSource({ "apple, 1", "banana, 2", "'lemon, lime', 3" })
        void testWithCustomDisplayNames(String fruit, int rank) {
        }



    }