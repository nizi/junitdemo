package com.uinnva.qa.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnJre;
import org.junit.jupiter.api.condition.EnabledOnOs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.condition.JRE.JAVA_8;
import static org.junit.jupiter.api.condition.JRE.JAVA_9;

public class JDKDemo {

    @Test
    @EnabledOnJre(JAVA_8)
    void jir8(){
        fail("study fail");
    }

    @Test
    @EnabledOnJre(JAVA_9)
    void jir9(){
        String msg = "helo";
        assertEquals(msg,"study fail");
    }
}
