# Git global setup
### git config --global user.name "wangnini"
### git config --global user.email "wangniniq@163.com"

# Create a new repository
### git clone git@gitlab.com:nizi/junitdemo.git
### cd junitdemo
### touch README.md
### git add README.md
### git commit -m "add README"
### git push -u origin master

# Push an existing folder
### cd existing_folder
### git init
### git remote add origin git@gitlab.com:nizi/junitdemo.git
### git add .
### git commit -m "Initial commit"
### git push -u origin master

# Push an existing Git repository
### cd existing_repo
### git remote rename origin old-origin
### git remote add origin git@gitlab.com:nizi/junitdemo.git
### git push -u origin --all
### git push -u origin --tags

hell